#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"

using std::cout;
using std::endl;

int main()
{
#pragma region MY_TREE_TEST
	BSNode* myTree = new BSNode("Finneas");
	myTree->insert("Billie");
	myTree->insert("Patrick");
	myTree->insert("Maggie");
	myTree->insert("Claudia");
	myTree->insert("Finneas");
	myTree->insert("Billie");
	myTree->insert("Patrick");
	myTree->insert("Maggie");
	myTree->insert("Claudia");

	// Test print function
	cout << "Printing myTree:" << endl;
	myTree->printNodes();
	cout << endl;

	// Test the search function
	cout << "Searching for \"Billie\": ";

	if (myTree->search("Billie"))
	{
		cout << "found!" << endl;
	}
	else
	{
		cout << "not found!" << endl;
	}

	cout << "Searching for \"Ariana\": ";

	if (myTree->search("Ariana"))
	{
		cout << "found!" << endl;
	}
	else
	{
		cout << "not found!" << endl;
	}
	cout << endl;

	delete myTree;
#pragma endregion

#pragma region BS_TREE_TEST
	BSNode* bs = new BSNode("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");

	cout << "Tree height: " << bs->getHeight() << endl;

	// Test depth function. Use try and catch because if the node is null, the function will throw an exception.
	try
	{
		cout << "depth of node with 5: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	}
	catch (string & e)
	{
		cout << e << endl;
	}

	try
	{
		cout << "depth of node with 3: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;
	}
	catch (string & e)
	{
		cout << e << endl;
	}	

	std::string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete bs;
#pragma endregion	

	return 0;
}

