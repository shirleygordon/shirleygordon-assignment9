#pragma once
#include <string>
#include <iostream>

using std::cout;
using std::endl;
using std::string;

#define NOT_A_CHILD -1

class BSNode
{
public:
	BSNode(string data);
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(string value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	std::string getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(string val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const; //for question 1 part C

private:
	string _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth
};