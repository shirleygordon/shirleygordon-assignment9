#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>

using std::string;
using std::cout;
using std::endl;

#define SIZE 15

int main()
{
#pragma region SORT_ARRAY_TEST
	unsigned int i = 0;
	string strArr[SIZE] = { "Finneas", "Claudia", "Billie", "Ariana", "Taylor", "Olly", "Bella", "Harry", "Sam", "Luke", "Mabel", "Lewis", "James" , "Khalid", "Halsey" };
	int intArr[SIZE] = { 7, 15, 104, 3, 123, 0, -3, -200, 456, 21, 109, 97, 13, -543, -1 };

	// Print the string array
	for (i = 0; i < SIZE; i++)
	{
		cout << strArr[i] << " ";
	}
	cout << endl << endl;

	// Print the int array
	for (i = 0; i < SIZE; i++)
	{
		cout << intArr[i] << " ";
	}
	cout << endl << endl;


	// Create a tree from the string array.
	BSNode<string>* strTree = new BSNode<string>(strArr[0]);
	for (i = 1; i < SIZE; i++)
	{
		strTree->insert(strArr[i]);
	}

	// Print the sorted string array
	cout << "Sorted string array:" << endl;
	strTree->printNodes();
	cout << endl;

	// Create a tree from the int array.
	BSNode<int>* intTree = new BSNode<int>(intArr[0]);
	for (i = 1; i < SIZE; i++)
	{
		intTree->insert(intArr[i]);
	}

	// Print the sorted int array
	cout << "Sorted int array:" << endl;
	intTree->printNodes();
	cout << endl;

	delete strTree;
	delete intTree;
#pragma endregion

#pragma region MY_TESTS

#pragma region STRING_TREE_TEST
	BSNode<string>* myTree = new BSNode<string>("Finneas");
	myTree->insert("Billie");
	myTree->insert("Patrick");
	myTree->insert("Maggie");
	myTree->insert("Claudia");
	myTree->insert("Finneas");
	myTree->insert("Billie");
	myTree->insert("Patrick");
	myTree->insert("Maggie");
	myTree->insert("Claudia");

	// Test print function
	cout << "Printing myTree:" << endl;
	myTree->printNodes();
	cout << endl;

	// Test the search function
	cout << "Searching for \"Billie\": ";

	if (myTree->search("Billie"))
	{
		cout << "found!" << endl;
	}
	else
	{
		cout << "not found!" << endl;
	}

	cout << "Searching for \"Ariana\": ";

	if (myTree->search("Ariana"))
	{
		cout << "found!" << endl;
	}
	else
	{
		cout << "not found!" << endl;
	}
	cout << endl;

	// Test getHeight function
	cout << "Tree height: " << myTree->getHeight() << endl;

	// Test getDepth function. Use try and catch because if the node is null, the function will throw an exception.
	try
	{
		cout << "Depth of node with \"Billie\": " << myTree->getLeft()->getDepth(*myTree) << endl;
	}
	catch (string & e)
	{
		cout << e << endl;
	}

	try
	{
		cout << "Depth of node with \"Finneas\": " << myTree->getDepth(*myTree) << endl;
	}
	catch (string & e)
	{
		cout << e << endl;
	}

	delete myTree;
#pragma endregion

#pragma region INT_TREE_TEST
	BSNode<int>* bs = new BSNode<int>(10);
	bs->insert(15);
	bs->insert(8);
	bs->insert(3);
	bs->insert(5);
	bs->insert(9);
	bs->insert(16);
	bs->insert(7);
	bs->insert(7);

	// Test print function
	cout << endl << "Printing bs tree:" << endl;
	bs->printNodes();
	cout << endl;

	// Test search function
	cout << "Searching for 5: ";

	if (bs->search(5))
	{
		cout << "found!" << endl;
	}
	else
	{
		cout << "not found!" << endl;
	}

	cout << "Searching for 13: ";

	if (bs->search(13))
	{
		cout << "found!" << endl;
	}
	else
	{
		cout << "not found!" << endl;
	}
	cout << endl;

	// Test getHeight function
	cout << "Tree height: " << bs->getHeight() << endl;

	// Test getDepth function. Use try and catch because if the node is null, the function will throw an exception.
	try
	{
		cout << "Depth of node with 5: " << bs->getLeft()->getLeft()->getRight()->getDepth(*bs) << endl;
	}
	catch (string & e)
	{
		cout << e << endl;
	}

	try
	{
		cout << "Depth of node with 16: " << bs->getRight()->getRight()->getDepth(*bs) << endl;
	}
	catch (string & e)
	{
		cout << e << endl;
	}
#pragma endregion

#pragma endregion

	system("pause");
	delete bs;

	return 0;
}

