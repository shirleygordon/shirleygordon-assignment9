#include "functions.h"
#include "test.h"
#include <iostream>

int main()
{ 
#pragma region TEST_DOUBLE
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;
#pragma endregion

#pragma region TEST_CHAR
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('a', 'b') << std::endl;
	std::cout << compare<char>('z', 'c') << std::endl;
	std::cout << compare<char>('b', 'b') << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	//const int arr_size = 5;
	char charArray[arr_size] = { 'f', 'b', 'm', 'p', 'c' };
	bubbleSort<char>(charArray, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << charArray[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArray, arr_size);
	std::cout << std::endl;
#pragma endregion

#pragma region TEST_CLASS

	test t1(1);
	test t2(2);
	test t3(3);

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<test>(t1, t2) << std::endl;
	std::cout << compare<test>(t3, t2) << std::endl;
	std::cout << compare<test>(t1, t1) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	//const int arr_size = 5;
	test testArray[arr_size] = { t3, t2, t1, t3, t2 };
	bubbleSort<test>(testArray, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << testArray[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<test>(testArray, arr_size);
	std::cout << std::endl;

#pragma endregion

	system("pause");
	return 1;
}