#include "BSNode.h"

/*
C'tor for binary search tree node.
Input: data string.
Output: none.
*/
BSNode::BSNode(string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}

/*
Copy c'tor for binary search tree node.
Input: node to copy reference.
Output: none.
*/
BSNode::BSNode(const BSNode& other)
{
	this->_data = other._data; // Shallow copy, because it's not a pointer.
	this->_count = other._count;
	this->_left = nullptr;
	this->_right = nullptr;
	
	if (other._left != nullptr) // Copy all left subtrees.
	{
		this->_left = new BSNode(*other._left);
	}
	
	if (other._right != nullptr) // Copy all right subtrees.
	{
		this->_right = new BSNode(*other._right);
	}
}

/*
D'tor for binary search tree.
Input: none.
Output: none.
*/
BSNode::~BSNode()
{
	if (!this->isLeaf())
	{
		if (this->_left != nullptr)
		{
			this->_left->~BSNode(); // Delete the left subtree of the current node
			delete this->_left;
			this->_left = nullptr;
		}

		if(this->_right != nullptr)
		{
			this->_right->~BSNode(); // Delete the right subtree of the current node
			delete this->_right;
			this->_right = nullptr;
		}
	}
}

/*
Function inserts a new value into the binary search tree.
Input: value string to insert.
Output: none.
*/
void BSNode::insert(string value)
{
	if (this->_data != value) // If the value isn't already in the tree 
	{
		// Search for the place to put value in the tree
		if (/*std::stoi(this->_data) > std::stoi(value)*/value.compare(this->_data) < 0) // Current node's data is larger than the value
		{
			if (this->_left != nullptr)
			{
				this->_left->insert(value); // Insert value in the current node's left subtree
			}
			else
			{
				BSNode* newNode = new BSNode(value); // Create new node 
				this->_left = newNode; // Put the node in the left subtree
			}
		}
		else if (/*std::stoi(this->_data) < std::stoi(value)*/value.compare(this->_data) > 0) // Current node's data is smaller than the value
		{
			if (this->_right != nullptr)
			{
				this->_right->insert(value); // Insert value in the current node's right subtree
			}
			else
			{
				BSNode* newNode = new BSNode(value); // Create new node 
				this->_right = newNode; // Put the node in the right subtree
			}
		}
	}
	else
	{
		this->_count++; // Increase count if the same data is entered again.
	}
}

/*
Function creates a deep copy of the input node and returns it.
Input: node to copy.
Output: new node, which is a copy of the input node.
*/
BSNode& BSNode::operator=(const BSNode& other)
{
	delete this; // Delete old node.
	BSNode* newNode = new BSNode(other); // Create new node using copy c'tor.

	return *newNode; 
}

/*
Function checks if the current node is a leaf.
Input: none.
Output: true if the node is a leaf, false otherwise.
*/
bool BSNode::isLeaf() const
{
	bool isLeaf = false;

	// If both sides of the node are null, the node is a leaf.
	if (this->_right == nullptr && this->_left == nullptr)
	{
		isLeaf = true;
	}

	return isLeaf;
}

/*
Function returns the data of the current node.
Input: none.
Output: data string.
*/
std::string BSNode::getData() const
{
	return this->_data;
}

/*
Function returns a pointer to the left subtree of the current node.
Input: none.
Output: BSNode pointer to the left subtree of the current node.
*/
BSNode* BSNode::getLeft() const
{
	return this->_left;
}

/*
Function returns a pointer to the right subtree of the current node.
Input: none.
Output: BSNode pointer to the right subtree of the current node.
*/
BSNode* BSNode::getRight() const
{
	return this->_right;
}

/*
Function searches for a value in the binary search tree.
Input: value string to search for.
Output: true if value was found, false otherwise.
*/
bool BSNode::search(string val) const
{
	bool found = false;

	if (this->_data == val) // If the current value is the value to search for, return true.
	{
		found = true;
	}
	else
	{
		if (this->_left != nullptr && /*std::stoi(val) < std::stoi(this->_data)*/val.compare(this->_data) < 0) // If the value is smaller than the current value
		{
			found = this->_left->search(val); // Search for the value in the left subtree.
		}
		else if (this->_right != nullptr && /*std::stoi(val) > std::stoi(this->_data)*/val.compare(this->_data) > 0) // If the value is bigger than the current value
		{
			found = this->_right->search(val); // Search for the value in the right subtree.
		}
	}
	return found;
}

/*
Function returns the height of the binary search tree.
Input: none.
Output: the height of the binary search tree.
*/
int BSNode::getHeight() const
{
	int leftHeight = 0, rightHeight = 0, height = 0;

	if (this != nullptr)
	{
		if (this->_left != nullptr) // Get the height of the left subtree.
		{
			leftHeight = this->_left->getHeight();
			leftHeight++;
		}
		
		if (this->_right != nullptr) // Get the height of the right subtree.
		{
			rightHeight = this->_right->getHeight();
			rightHeight++;
		}
	}

	// Return the bigger height.
	if (leftHeight > rightHeight) 
	{
		height = leftHeight;
	}
	else
	{
		height = rightHeight;
	}

	return height;
}

/*
Function returns the depth of a node in the binary search tree.
Input: binary search tree root reference.
Output: the depth of the tree.
*/
int BSNode::getDepth(const BSNode& root) const
{
	int depth = 0;

	if (this != nullptr)
	{
		if (root.search(this->_data)) // Current node is a child of the root node.
		{
			depth = this->getCurrNodeDistFromInputNode(&root); // Get the depth.
		}
		else // Current node is not a child of the root node.
		{
			depth = NOT_A_CHILD; // Return error code (-1).
		}
	}
	else
	{
		throw string("Node is null. Unable to find depth.");
	}
	return depth;
}

/*
Function prints tree nodes in alphabetical order.
Input: none.
Output: none.
*/
void BSNode::printNodes() const
{
	if (this != nullptr)
	{
		// Print the left subtree
		if (this->_left != nullptr)
		{
			this->_left->printNodes();
		}

		// Print the root
		cout << this->_count << " " << this->_data << endl;

		// Print the right subtree
		if (this->_right != nullptr)
		{
			this->_right->printNodes();
		}
	}
}

/*
Function returns the distance of the current node from the input node.
Input: node pointer.
Output: distance of the current node from the input node.
*/
int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	int depth = 0;

	if (this != nullptr)
	{
		if (this->_data != node->_data)
		{
			if (/*std::stoi(this->_data) < std::stoi(node->_data)*/this->_data.compare(node->_data) < 0) // If the current node's data is smaller than the input node's,
			{
				depth = this->getCurrNodeDistFromInputNode(node->_left); // Search for it in the left subtree.
				depth++;
			}
			else if (/*std::stoi(this->_data) > std::stoi(node->_data)*/this->_data.compare(node->_data) > 0) // If the current node's data is bigger than the input node's,
			{
				depth = this->getCurrNodeDistFromInputNode(node->_right); // Search for it in the right subtree.
				depth++;
			}
		}
	}

	return depth;
}
