#pragma once
#include <iostream>

using std::ostream;

class test
{
public:
	int _test;

	test() { _test = 0; };
	test(int val) { _test = val; };
	~test() {};

	friend bool operator<(const test& first, const test& second);
	friend bool operator==(const test& first, const test& second);
	friend ostream& operator<<(ostream& output, const test& t);
};