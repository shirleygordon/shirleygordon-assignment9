#pragma once
#include <iostream>

using std::cout;
using std::endl;

#define EQUAL 0
#define BIGGER -1
#define SMALLER 1

/*
Template function for comparing 2 variables.
Input: reference to 2 variables.
Output: -1 if the first variable is bigger than the second one,
		1 if the first variable is smaller than the second one,
		and 0 if they're equal.
*/
template <class T>
int compare(T first, T second)
{
	int result = BIGGER;

	if (first < second)
	{
		result = SMALLER;
	}
	else if (first == second)
	{
		result = EQUAL;
	}

	return result;
}

/*
Template function for sorting an array using bubble sort.
Input: array, size of array.
Output: none.
*/
template <class T>
void bubbleSort(T arr[], int size)
{
	T temp;
	bool swapsOccurred = true;
	unsigned int i = 0;

	while (swapsOccurred)
	{
		swapsOccurred = false;

		for (i = 0; i < size - 1; i++)
		{
			// If the element is bigger than the following element,
			if (compare(arr[i], arr[i + 1]) == BIGGER)
			{
				// swap them.
				temp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = temp;

				swapsOccurred = true;
			}
		}

		size--; // No need to sort the last element, it's already the largest.
	}
}

/*
Template function for printing an array, each element in a separate line.
Input: array, size of array.
Output: none.
*/
template <class T>
void printArray(T arr[], const int& size)
{
	unsigned int i = 0;

	for (i = 0; i < size; i++)
	{
		cout << arr[i] << endl;
	}
}