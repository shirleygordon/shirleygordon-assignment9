#include "test.h"

bool operator<(const test& first, const test& second)
{
	return first._test < second._test;
}

bool operator==(const test& first, const test& second)
{
	return first._test == second._test;
}

ostream& operator<<(ostream& output, const test& t)
{
	output << t._test;
	return output;
}
